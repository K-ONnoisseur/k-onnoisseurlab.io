---
layout: review
title: K-On!
status: Watched
score: 10/10
img: img/k-on.jpg
imgsrc: ANN
---

The beginning of it all. Yui runs to the school, *and the rest is history*.
