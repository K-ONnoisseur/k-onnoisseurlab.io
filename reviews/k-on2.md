---
layout: review
title: K-On!!
status: Watched
score: 10/10
img: img/k-on2.jpg
imgsrc: ANN
---

Season 2 brings out a refreshing intro in its first cour, and a banger in the second cour. The cinematography is amazing. The girls are top tier (that's a given and I really shouldn't have mentioned that). Tenshi ni Fureta yo is also the perfect send-off for Azunyan. I love it. I love it so fucking much.
