---
layout: default
---

### K-ONnoisseur's Anime List

Welcome to K-ONnoisseur's Anime List. This unremarkable site gives you literally nothing but praises at K-ON. I'm serious. I won't even give bullshit "deep" reviews like those pieces of shit at MAL who gave less than 9/10 to K-ON since I'm not Roger Ebert. Don't say I didn't warn you.

Here are my reviews for:
- [K-On!](/reviews/k-on)
- [K-On!!](/reviews/k-on2)
- [K-On! Movie](/reviews/k-onmovie)

The only reason I don't use MAL anymore is I forgot my password. I liked this one anyway since it's dead simple and isn't bloated. It only takes ~1.5KiB to load this page, and including images in respective "reviews" I have, they only take ~21KiB each. It's nifty, and completely customizable to my needs. [Fork it on Gitlab!](https://gitlab.com/k-onnoisseur/k-onnoisseur.gitlab.io)
